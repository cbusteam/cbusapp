package dublinjerometeam3.cbusapp;

import android.app.Activity;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Michael on 4/4/2017.
 */

/**
 * A class for convenient management of placetypes in the settings menu
 */
class PlaceTypesWrapper{

    private boolean isChecked;
    private String PlaceTypeName;
    SharedPreferences settings;

    PlaceTypesWrapper(SharedPreferences prefs){
        settings = prefs;
    }

    private PlaceTypesWrapper(String name, boolean checked){
        isChecked=checked;
        PlaceTypeName=name;
    }

    List<PlaceTypesWrapper> getListOfPlaceTypes(){
        List<PlaceTypesWrapper> types = new ArrayList<>();
        types.add(new PlaceTypesWrapper("accounting",getCheckedStateFromMemory("accounting")));
        types.add(new PlaceTypesWrapper("airport",getCheckedStateFromMemory("airport")));
        types.add(new PlaceTypesWrapper("amusement_park",getCheckedStateFromMemory("amusement_park")));
        types.add(new PlaceTypesWrapper("book_store",getCheckedStateFromMemory("book_store")));
        types.add(new PlaceTypesWrapper("city_hall",getCheckedStateFromMemory("city_hall")));
        types.add(new PlaceTypesWrapper("florist",getCheckedStateFromMemory("florist")));
        types.add(new PlaceTypesWrapper("gym",getCheckedStateFromMemory("gym")));
        types.add(new PlaceTypesWrapper("library", getCheckedStateFromMemory("library")));
        types.add(new PlaceTypesWrapper("museum",getCheckedStateFromMemory("museum")));
        types.add(new PlaceTypesWrapper("park",getCheckedStateFromMemory("park")));
        types.add(new PlaceTypesWrapper("university",getCheckedStateFromMemory("university")));

        return types;
    }

    private boolean getCheckedStateFromMemory(String placeName){
        return settings.getBoolean(placeName,false);
    }

    public String getName(){
        return PlaceTypeName;
    }

    boolean getCheckState(){
        return isChecked;
    }
    void setChecked(boolean state){
        isChecked=state;
    }
}
