package dublinjerometeam3.cbusapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michael on 4/2/2017.
 */

class PlaceIDListAdapter extends BaseExpandableListAdapter {

	private ArrayList<String> groups;
	final private List<List<PlaceTypesWrapper>> placeTypes;
	private LayoutInflater inflator;

	PlaceIDListAdapter(Context context,ArrayList<String> groups, List<List<PlaceTypesWrapper>> placeTypesData){
		this.groups=groups;
		placeTypes = placeTypesData;
		inflator=LayoutInflater.from(context);
	}

	List<PlaceTypesWrapper> getChildStates(){		//returns null? TODO Ask Michael later.
		return placeTypes.get(0);
	}

	@Override
	public View getChildView(final int i, final int i1, boolean b, View view, ViewGroup viewGroup) {
		View v;
		v=inflator.inflate(R.layout.checkedchilditem,viewGroup,false);
			PlaceTypesWrapper place= placeTypes.get(i).get(i1);
			String placeName = place.getName();
			boolean isChecked = place.getCheckState();
			CheckBox box = (CheckBox) v.findViewById(R.id.placetypebox);
			box.setText(placeName);
			box.setChecked(isChecked);
			box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
					placeTypes.get(i).get(i1).setChecked(b);
				}
			});
			return v;
	}

	@Override
	public boolean isChildSelectable(int i, int i1) {
		return true;
	}   //fulfill abstract methods

	@Override
	public int getGroupCount() {
		return groups.size();
	}

	@Override
	public int getChildrenCount(int i) {
		return placeTypes.get(i).size();
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}		//fulfilling more abstract methods

	@Override
	public Object getChild(int i, int i1) {
		return placeTypes.get(i).get(i1);
	}

	@Override
	public long getGroupId(int i) {
		return i*5;
	}

	@Override
	public Object getGroup(int i) {
		return groups.get(i);
	}

	@Override
	public long getChildId(int i, int i1) {
		return (i*5)+i1;
	}

	@Override
	public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
		View v;
			v = inflator.inflate(R.layout.groupheading, viewGroup,false);
			String gt = (String)getGroup(i);
			TextView heading = (TextView) v.findViewById(R.id.heading);
			if(gt!=null){
				heading.setText(gt);
			}
			return v;
	}
}
