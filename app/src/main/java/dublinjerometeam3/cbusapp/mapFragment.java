package dublinjerometeam3.cbusapp;

/**
 * Created by Michael on 4/15/2017.
 */

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.MarkerManager;
import com.google.maps.android.SphericalUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * A Class for the fragment containing our maps and other controls
 */
public class mapFragment extends Fragment implements OnMapReadyCallback{

    /**
     * The parent activity (HubActivity) of this fragment
     */
    FragmentActivity parentActivity;

    /**
     * The google map used buy this fragment
     */
    GoogleMap userMap;

    /**
     * Provides access to the GPS sensor on the device and methods to request location updates
     */
    LocationManager locationManager;

    /**
     * True if the map should follow the user's location when their location changes
     */
    boolean autoFollow;

    /**
     * The user's current lat and long
     */
    double latitude;
    double longitude;


    /**
     * The button for showing the newRouteDialog
     */
    FloatingActionButton newRouteButton;

    /**
     * The dialog fragment for generating new routes
     */
    NewRouteDialogFragment newRouteDialog;

    /**
     * The Google Map marker (waypoint) the user is currently progressing towards
     */
    Marker currentMarker;

    /**
     * A list of markers that are in the current route
     */
    List<Marker> markers;

    /**
     * Information about the marker the user is progressing towards and the current section
     * the user is in
     */
    MarkerManager markerManager;
    int currentMarkerIndex;
    int currentSection;

    /**
     * polyLines conatains a polyLine for each section
     * sectionLatLngs contains all the points used in the route to generate all the polylines in
     * the route
     */
    List<Polyline> polyLines=new ArrayList<>();
    List<List<LatLng>> sectionLatLngs = new ArrayList<>();

    /**
     * True if the user's location has been found and is a real number
     */
    boolean isLocationFound = false;

    /**
     * The Volley RequestQueue for making http requests
     */
    RequestQueue requestQueue;

    /**
     *
     */
    List<List<PlaceTypesWrapper>> placeSettingsData = new ArrayList<>();

    /**
     * The overlay that prevents the user from interacting with the map if the GPS can't update
     */
    RelativeLayout loadingOverlay;

    /**
     * Data for routes completed and the distance the user has travelled
     */
    int routesCompleted=0;
    double distTravelled=0;

    /**
     * True if user has granted permission for their location to be received
     */
    boolean isPermissionGranted = false;

    //Empty constructor needed
    public mapFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentActivity = getActivity();
        Cache cache = new DiskBasedCache(parentActivity.getCacheDir(),1024*1024);
        Network network = new BasicNetwork(new HurlStack());
        requestQueue = new RequestQueue(cache,network);
        requestQueue.start();
        return inflater.inflate(R.layout.activity_maps2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        newRouteDialog = new NewRouteDialogFragment();
        if (userMap == null) {
            //Gets a reference to the map CONTAINER fragment, which consists only of the Google Map, this not the map fragment as a whole
            SupportMapFragment supportMapFragment = (SupportMapFragment)
                    getChildFragmentManager().findFragmentById(R.id.usingmap);
            //If reference found above
            if (supportMapFragment != null) {
                //Since we know the Google Map has been created and inflated, we can now start to get the data for the map using getMapAsync
                startGetMapAsync(supportMapFragment);
                //We can also get references to views in the Maps menu since the entire layout exists
                newRouteButton = (FloatingActionButton)
                        view.findViewById(R.id.floatingActionButton);
                loadingOverlay=(RelativeLayout) view.findViewById(R.id.loadingoverlay);
            }
        }
    }

    public void doMapActivities(){
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 2, locationListener);

            userMap.setMyLocationEnabled(true);
            userMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    double dist;
                    for(int i=currentMarkerIndex; i < markers.size();i++){
                        if(markers.get(i).getPosition().longitude==marker.getPosition().longitude && markers.get(i).getPosition().latitude == marker.getPosition().latitude){

                            List<Marker> temp = markers.subList(currentMarkerIndex,i+1);
                            i = markers.size();
                            List<LatLng> tempCoords = new ArrayList<>();
                            tempCoords.add(new LatLng(latitude,longitude));

                            for(Marker tempMarker : temp){
                                tempCoords.add(tempMarker.getPosition());
                            }

                            dist = SphericalUtil.computeLength(tempCoords);
                            marker.setTitle(marker.getTitle()!=null ? marker.getTitle() : "Checkpoint marker: " + marker.getPosition().toString());
                            marker.setSnippet((int)dist+" meters to this location");
                        }
                    }
                    marker.showInfoWindow();
                    return true;
                }
            });
            userMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    if(!marker.getTitle().contains("Checkpoint marker") && !(marker.getTitle()==null)) {
                        String[] temp = marker.getTitle().split(": ");
                        Uri uri = Uri.parse("http://www.google.com/#q=" + temp[0]);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                }
            });
            userMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    userMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(new LatLng(latitude,longitude),100,80,0)));
                    if(!autoFollow){
                        autoFollow = true;
                    }
                    return true;
                }
            });
            userMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    if(autoFollow){
                        autoFollow=false;
                    }
                }
            });
        }catch(SecurityException e){
            e.printStackTrace();
        }
        if (newRouteButton != null) {
            newRouteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentManager fm = parentActivity.getSupportFragmentManager();
                    newRouteDialog.show(fm,"New Route");
                }
            });
        }
    }

    /**
     * Called when the google map is ready
     * @param googleMap the google map that was generated
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        userMap = googleMap;

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
        }
        else{
            isPermissionGranted = true;
            doMapActivities();
        }
    }

    /**
     * Called when user clicks 'Generate new route'
     * @param distance radius to search within in meters (limited to 1000)
     * @param numOfLocations number of places to include in route (limited to 10; too many waypoints)
     *                       slows the app down because of memory shortages
     */
    public void generateRoute(int distance, int numOfLocations) {
        userMap.clear();
        currentMarkerIndex=-1;
        currentMarker = null;
        markers=null;

        HubActivity instance = (HubActivity) parentActivity;
        placeSettingsData = instance.getUserPlaceTypesFromFragmentInstance();
        if(newRouteDialog != null && newRouteDialog.isVisible()){
            newRouteDialog.dismiss();
        }
        List<String> placetypes= new ArrayList<>();
        for(PlaceTypesWrapper place : placeSettingsData.get(0)){
            if(place.getCheckState()) {
                placetypes.add(place.getName());
            }
        }
        requestPlaces(distance,placetypes,numOfLocations);
    }

    /**
     * Called when the user either grants or denys a permission, or when the app finishes checking
     * permissions already stored in memory
     * @param requestCode the request code made by the initial permissions request
     * @param permissions the permissions requested
     * @param grantResults the results indicating if each permission was granted or denied
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
            isPermissionGranted=true;
            doMapActivities();
        }
    }

    /**
     * Makes a request to the Google Directions API servers and, if a valid response is returned,
     * calls drawRoute
     * @param destination a MarkerOptions object containing information about the destination location
     * @param waypoints the waypoints that should be included in the route
     */
    private void requestDirections(final MarkerOptions destination, final List<MarkerOptions> waypoints){
        if(!Double.isNaN(latitude) && !Double.isNaN(longitude)) {
            String url;
            if(waypoints.size()>0){

                String waypointsUrlString = "";

                for(int i=0; i<waypoints.size();i++){
                    if(i==waypoints.size()-1){
                        waypointsUrlString+=waypoints.get(i)
                                .getPosition()
                                .latitude
                                + "," + waypoints.get(i).getPosition().longitude;
                    }
                    else{
                        waypointsUrlString += waypoints.get(i).getPosition().latitude + "," + waypoints.get(i).getPosition().longitude + "|";
                    }
                }
                url = "https://maps.googleapis.com/maps/api/directions/json?" +
                        "origin=" + latitude + "," + longitude +
                        "&destination="+destination.getPosition().latitude+","+destination.getPosition().longitude +
                        "&mode=walking" +
                        "&waypoints=" + waypointsUrlString +
                        "&optimize=true"+
                        "&key=AIzaSyAVEgUi65FGCDfCcfK7Xh162xZoiefaq0g";
            }
            else {
                url = "https://maps.googleapis.com/maps/api/directions/json?" +
                        "origin=" + latitude + "," + longitude +
                        "&destination="+destination.getPosition().latitude+","+destination.getPosition().longitude +
                        "&mode=walking" +
                        "&key=AIzaSyAVEgUi65FGCDfCcfK7Xh162xZoiefaq0g";
            }

            JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("Direction response", response.toString());
                    drawRoute(response,waypoints,destination);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Request error", error.getMessage());
                }
            });

            requestQueue.add(jsonArrayRequest);
        }
    }

    //http://stackoverflow.com/questions/19506561/convert-polyline-to-route
    //thanks to user2919740 for this decoder
    /**
     * Decodes a polyline string into a list of points
     * @param encoded An encoded polyline string
     * @return A list of latlng points
     */
    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng( (((double) lat / 1E5)),
                    (((double) lng / 1E5) ));
            poly.add(p);
        }
        return poly;
    }

    /**
     * Finds the bounds of a series of points to fit the map viewport
     * @param points A List of points to bound by
     * @return A LatLngBounds object that contains the bounds of the viewport
     */
    private LatLngBounds getBounds(List<LatLng> points){
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for(LatLng point : points){
            builder.include(point);
        }
        return builder.build();
    }

    /**
     * Draws all the lines and waypoint markers on the map
     * @param data The data to be used for route info
     * @param waypoints A list of waypoints corresponding to the places picked by the route generator
     * @param dest A LatLng of the destination
     */
    private void drawRoute(JSONObject data,List<MarkerOptions> waypoints,MarkerOptions dest){
        userMap.clear();
        currentMarkerIndex=-1;
        currentMarker = null;

        if(markers!=null){markers.clear();}
        if(polyLines!=null){polyLines.clear();}
        if(sectionLatLngs!=null){sectionLatLngs.clear();}

        markers=new ArrayList<>();
        polyLines=new ArrayList<>();
        sectionLatLngs=new ArrayList<>();

        try {
            markerManager=new MarkerManager(userMap);
            JSONArray legs=data.getJSONArray("routes").getJSONObject(0).getJSONArray("legs");

            List<PatternItem> pattern = Arrays.asList(new Dot(), new Gap(20));
            List<LatLng> allPoints = new ArrayList<>();

            for(int i=0; i < legs.length();i++){
                JSONArray steps = legs.getJSONObject(i).getJSONArray("steps");
                int stepslen = steps.length();
                List<LatLng> temp=new ArrayList<>();

                if (i==0){
                    for(int j =0; j<stepslen;j++){
                        List<LatLng> stepPolyline = decodePoly(steps.getJSONObject(j).getJSONObject("polyline").getString("points"));
                        for(LatLng point:stepPolyline){
                            temp.add(point);
                            allPoints.add(point);
                        }
                    }
                    polyLines.add(userMap.addPolyline(new PolylineOptions().addAll(temp).pattern(pattern).color(Color.BLUE).width(20)));
                }
                else{
                    for(int j =0; j<stepslen;j++){
                        List<LatLng> stepPolyline = decodePoly(
                                steps.getJSONObject(j).getJSONObject("polyline").getString("points"));
                        for(LatLng point:stepPolyline){
                            temp.add(point);
                            allPoints.add(point);
                        }
                    }

                    polyLines.add(
                            userMap.addPolyline(
                                    new PolylineOptions()
                                            .addAll(temp)
                                            .pattern(pattern)
                                            .color(Color.GRAY)
                            )
                    );

                }

                sectionLatLngs.add(temp);
            }

            autoFollow=false;
            userMap.animateCamera(CameraUpdateFactory.newLatLngBounds(getBounds(allPoints),20));
        }

        catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i =0; i<sectionLatLngs.size();i++){

            boolean isEnd =false;

            for (int j = 0; j < sectionLatLngs.get(i).size(); j+=4) {

                MarkerOptions options = new MarkerOptions();

                if(j+4>=sectionLatLngs.get(i).size()){

                    if(!(waypoints.size()==i)) {
                        options.title(waypoints.get(i).getTitle() + ": " + waypoints.get(i).getPosition().toString());
                    }

                    isEnd=true;

                    options
                            .position(sectionLatLngs.get(i)
                                    .get(sectionLatLngs
                                            .get(i)
                                            .size()-1
                                    )
                            )

                            .icon(
                                    BitmapDescriptorFactory.defaultMarker(
                                            BitmapDescriptorFactory.HUE_ORANGE
                                    )
                            );
                }
                else {
                    options.position(sectionLatLngs.get(i).get(j));
                }
                markers.add(userMap.addMarker(options));

                if(!isEnd && i!=0) {
                    markers.get(markers.size() - 1).remove();
                }
            }
        }
        currentMarker = markers.get(0);
        currentMarkerIndex=0;
        currentSection=0;

        markerManager=new MarkerManager(userMap);

        markers.add(
                userMap.addMarker(
                        new MarkerOptions()
                                .position(dest.getPosition())
                                .title(dest.getTitle()+": " + dest.getPosition().toString())
                                .icon(
                                        BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                )
        );
    }

    /**
     * Makes a request to the Google Places API
     * @param radius radius (in meters) to search within
     * @param placeTypes the types of places to search for (searches for all if the list is empty)
     * @param numOfWaypoints the number of waypoints to get
     */
    private void requestPlaces(final int radius, List<String> placeTypes, final int numOfWaypoints){
        String places = "";
        int numOfTypes = placeTypes.size();
        if(numOfTypes>0) {

            if(numOfTypes==1) {places+="&type=";}

            else {places+="&types=";}

            for (int i = 0; i < placeTypes.size(); i++) {

                if (i == placeTypes.size() - 1) {
                    places += placeTypes.get(i);
                }

                else {
                    places += placeTypes.get(i) + "|";
                }
            }
        }

        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                "location="+latitude+","+longitude+
                "&radius="+radius
                +places+
                "&key=AIzaSyBZ3Wcsk4kbsrB-af4jIaL2QFrxBpcMP3Y";
        Log.i("url",url);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("status").equals("ZERO_RESULTS") || response.getString("status").equals("INVALID_REQUEST")){
                        Toast.makeText(parentActivity.getApplicationContext(),"No places found!",Toast.LENGTH_LONG).show();
                    }

                    else {
                        Random rand = new Random();
                        JSONArray tempArr = response.getJSONArray("results");
                        List<MarkerOptions> waypoints = new ArrayList<>();
                        int wayPointsFound = 0;
                        boolean isDestFound = false;
                        MarkerOptions destLocation=null;

                        while (wayPointsFound!=numOfWaypoints && tempArr.length()>0) {
                            int randomLocationIndex = rand.nextInt(tempArr.length());
                            if(!(SphericalUtil.computeDistanceBetween(getPlaceLatLng(response.getJSONArray("results"), randomLocationIndex),new LatLng(latitude,longitude))>radius)){

                                if(!isDestFound){
                                    LatLng destLocationCoords = getPlaceLatLng(
                                            response.getJSONArray("results"),
                                            randomLocationIndex
                                    );

                                    destLocation = new MarkerOptions()
                                            .position(destLocationCoords)
                                            .title(
                                                    response.getJSONArray("results")
                                                            .getJSONObject(randomLocationIndex)
                                                            .getString("name")
                                            );

                                    isDestFound=true;
                                }

                                else {
                                    wayPointsFound++;
                                    waypoints.add(new MarkerOptions()

                                            .position(
                                                    getPlaceLatLng(
                                                            response.getJSONArray("results"),
                                                            randomLocationIndex)
                                            )
                                            .title(
                                                    response
                                                            .getJSONArray("results")
                                                            .getJSONObject(randomLocationIndex)
                                                            .getString("name")
                                            )
                                    );
                                }
                            }

                            tempArr.remove(randomLocationIndex);
                        }


                        if(waypoints.size()!=0){requestDirections(destLocation, waypoints);}
                        else{Toast.makeText(getActivity().getApplicationContext(),"No places found!",Toast.LENGTH_LONG).show();}
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(parentActivity.getApplicationContext(),"Something went wrong with the network.",Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(request);
    }

    /**
     * Gets the LatLng of the specified index in a JSON array
     * @param arr the JSONArray containing data of places
     * @param index the index to get the LatLng of
     * @return the LatLng of the given index in the array
     * @throws JSONException thrown if there is a parsing error
     */
    private LatLng getPlaceLatLng(JSONArray arr,int index) throws JSONException {
        double lat = Double.parseDouble(
                arr.getJSONObject(index)
                        .getJSONObject("geometry")
                        .getJSONObject("location")
                        .get("lat")
                        .toString()
        );

        double lng = Double.parseDouble(
                arr.getJSONObject(index)
                        .getJSONObject("geometry")
                        .getJSONObject("location")
                        .get("lng")
                        .toString()
        );
        return new LatLng(lat,lng);
    }

    /**
     * Provides methods for dealing with GPS changes
     */
    private final LocationListener locationListener = new LocationListener() {

        /**
         * Called when GPS location changes
         * @param location the new Location of the device
         */
        @Override
        public void onLocationChanged(Location location) {

            longitude = location.getLongitude();
            latitude = location.getLatitude();

            if(autoFollow){
                userMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(new LatLng(latitude,longitude),95,80,userMap.getCameraPosition().bearing)));
            }

            if(!isLocationFound){
                isLocationFound=true;
                newRouteButton.setClickable(true);
                loadingOverlay.setVisibility(View.GONE);
            }
            if(currentMarker!=null && SphericalUtil.computeDistanceBetween(new LatLng(latitude,longitude),currentMarker.getPosition())<=10){

                if(currentMarkerIndex!=0){
                    distTravelled += SphericalUtil.computeDistanceBetween(markers.get(currentMarkerIndex).getPosition(),markers.get(currentMarkerIndex-1).getPosition());
                }

                if(currentMarker==markers.get(markers.size()-2)){
                    currentMarker=null;
                    routesCompleted++;
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("Route complete! Total distance: "+distTravelled+" meters.").setIcon(android.R.drawable.checkbox_on_background).show();
                }

                else{
                    currentMarker.remove();

                    if(markers.get(currentMarkerIndex).getPosition().equals(sectionLatLngs.get(currentSection).get(sectionLatLngs.get(currentSection).size()-1))){
                        polyLines.get(currentSection).setColor(Color.GRAY);
                        polyLines.get(currentSection).setWidth(1);

                        currentSection++;
                        currentMarkerIndex++;

                        int tempIndex = currentMarkerIndex;
                        for(int i=0; i<sectionLatLngs.get(currentSection).size();i+=4){
                            MarkerOptions tempOptions = new MarkerOptions().position(markers.get(tempIndex).getPosition()).snippet(markers.get(tempIndex).getSnippet()).title(markers.get(tempIndex).getTitle());
                            markers.set(tempIndex,userMap.addMarker(tempOptions));
                            tempIndex++;
                        }
                        polyLines.get(currentSection).setColor(Color.BLUE);
                        polyLines.get(currentSection).setWidth(20);
                    }
                    currentMarkerIndex++;
                    currentMarker=markers.get(currentMarkerIndex);


                }

            }
        }

        /**
         * called when the GPS enters a new state
         * @param s the GPS provider
         * @param i the status of the GPS
         *          0: if the GPS is OUT_OF_SERVICE, and is not expected to come
         *          online soon
         *
         *          1: if the GPS is TEMPORARILY_UNAVAILABLE, and is expected to be online
         *          soon
         *
         *          2: if the GPS is AVAILABLE
         *
         * @param bundle Additional info
         */
        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            if(i==2){
                if(!isLocationFound){
                    isLocationFound=true;
                    newRouteButton.setClickable(true);
                    loadingOverlay.setVisibility(View.GONE);
                }
            }
        }

        /**
         * Called when GPS is enabled
         * @param s the GPS provider name
         */
        @Override
        public void onProviderEnabled(String s) {
        }

        /**
         * Called when GPS is turned off
         * @param s the name of the GPS provider
         */
        @Override
        public void onProviderDisabled(String s) {
            isLocationFound=false;
            newRouteButton.setClickable(false);
            loadingOverlay.setVisibility(View.VISIBLE);
        }
    };

    /**
     * Starts getting the data for a SupportMapFragment
     * @param supportMapFragment the fragment to load the map data to
     */
    public void startGetMapAsync(SupportMapFragment supportMapFragment) {
        supportMapFragment.getMapAsync(this);
    }

    public List<Marker> getMarkers(){
        return markers;
    }

    public int getCurrentMarkerIndex(){
        return currentMarkerIndex;
    }

    public int getMaxMarkerIndex(){
        return markers.size()-1;
    }

    public LatLng getUserLoc(){
        return new LatLng(latitude,longitude);
    }

    public void update(int newMarkerIndex, Polyline line, List<Marker> markers){

    }

    public boolean isPermissionGranted(){
        return isPermissionGranted;
    }
}
