package dublinjerometeam3.cbusapp;

/**
 * Created by Michael on 4/15/2017.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.List;

/**
 * A class for our settings menu
 */
public class SettingsFragment extends Fragment {
    FragmentActivity parentActivity;
    ArrayList<String> groupNames = new ArrayList<>();
    List<List<PlaceTypesWrapper>> placeSettingsData = new ArrayList<>();
    PlaceIDListAdapter listAdapter;
    ExpandableListView placesTypesList;
    public SettingsFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        parentActivity = getActivity();
        return inflater.inflate(R.layout.settings,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //if(placesTypesList == null){
        groupNames.add("Place Types");
        PlaceTypesWrapper wrapper = new PlaceTypesWrapper(parentActivity.getSharedPreferences("settings",0));
        placeSettingsData.add(wrapper.getListOfPlaceTypes());
            placesTypesList = (ExpandableListView) view.findViewById(R.id.placetypeslist);
        listAdapter = new PlaceIDListAdapter(parentActivity.getApplicationContext(),groupNames,placeSettingsData);
        placesTypesList.setAdapter(listAdapter);
            Log.i("a","a");
            Button applyButton = (Button) view.findViewById(R.id.apply);
            applyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    writeStatesToMemory(listAdapter.getChildStates());
                }
            });
        //}
        //Create new PlaceTypesWrapper passing it a preferences file named 'settings'

        //Setting the header for the place types expandable list in the settings menu
        //create new listAdapter that acts as an interface between our list data and the actual list views
        //listAdapter.notifyDataSetChanged();
    }

    void writeStatesToMemory(List<PlaceTypesWrapper> data){
        for(PlaceTypesWrapper type : data){
            parentActivity.getSharedPreferences("settings",0)
                    .edit()
                    .putBoolean(
                            type.getName(),type.getCheckState()
                    )
                    .apply();
        }
    }

    public List<List<PlaceTypesWrapper>> getUserPlaceTypes(){
        return placeSettingsData;
    }
}
