package dublinjerometeam3.cbusapp;

/**
 * Created by Michael on 4/17/2017.
 */

/**
 * Class representing a day of the year
 */
class DayHealthData {
    /**
     * All 3 of these variables are the individual pieces of data contained in each day
     */
    int steps=0;
    int routesCompleted=0;
    double distanceTravelled=0;
}
