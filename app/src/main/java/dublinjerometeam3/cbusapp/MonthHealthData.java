package dublinjerometeam3.cbusapp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Michael on 4/17/2017.
 */

public class MonthHealthData {
    public List<DayHealthData> daysInMonthData = new ArrayList<>();
    public int totalSteps=0;
    public double totalDistTravelled=0;
    public int totalRoutesCompleted=0;
}
