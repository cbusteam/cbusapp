package dublinjerometeam3.cbusapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by Michael on 3/20/2017.
 */
//handles the dialog for creating a new route
public class NewRouteDialogFragment extends DialogFragment{
    RouteButtonClickListener listener;
    public NewRouteDialogFragment(){

    }

    public interface RouteButtonClickListener{
        public void onButtonClick(int distance, int numOfLocations);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = layoutInflater.inflate(R.layout.newroutedialog, container, false);
        getDialog().setTitle("New Route");
        final TextView dist = (TextView) rootView.findViewById(R.id.distanceshower);        //distance in route
        final Button newRouteButton= (Button) rootView.findViewById(R.id.newroutebutton);   //button to generate a new route
        final SeekBar bar = (SeekBar) rootView.findViewById(R.id.distanceselector);         //????
        final SeekBar locsBar = (SeekBar) rootView.findViewById(R.id.numoflocationsbar);    //????
        final TextView locsText = (TextView) rootView.findViewById(R.id.numoflocationstext);//????
        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                dist.setText(getString(R.string.distanceSeekBarText,i+10));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        locsBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                locsText.setText(getString(R.string.numoflocations,i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        newRouteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("distance", ""+bar.getProgress());
                listener = (RouteButtonClickListener) getActivity();
                listener.onButtonClick(bar.getProgress(),locsBar.getProgress());
            }
        });
        return rootView;
    }
}
