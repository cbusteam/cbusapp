package dublinjerometeam3.cbusapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Our main app activity. It includes 3 'mini-activities' (called fragments) that each correspond
 * to a different tab of our app.
 */
public class HubActivity extends FragmentActivity implements SensorEventListener, NewRouteDialogFragment.RouteButtonClickListener{

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Here object refs are created for each of the 3 fragments. This does not inflate their views.
         * It only creates objects that can be used later
         * @param position The fragment at the specified index
         * @return The fragment instantiated
         */
        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                tabFragment =new TabFragment();
                return tabFragment;
            } else if (position == 1) {
                myMapFrag = new mapFragment();
                return myMapFrag;
            } else if(position==2){
                settingsFragment = new SettingsFragment();
                return settingsFragment;
            } else {
                return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }

    /**
     * The three fragments that make up the app
     */
    TabFragment tabFragment;
    mapFragment myMapFrag;
    SettingsFragment settingsFragment;

    /**
     * The stepDetector sensor used by this activity
     */
    SensorManager mSensorManager;
    Sensor stepDetector;
    int currentNumOfSteps=0;

    /**
     * The SharedPreferences file where health data is stored
     */
    SharedPreferences dataPrefs;

    /**
     * Used for (de)/serializing objects
     */
    Gson gson = new Gson();

    /**
     * Variables for the background service used when the user turns off the screen or open another
     * app
     */
    BackGroundService backGroundService;
    boolean isServiceBound = false;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO: 4/23/2017 handleService
        }
    };

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    Menu navigationMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Transition from splash screen to main activity
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hub);

        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        stepDetector = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            //Implemented
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            /**
             * sets tab highlighting for selected tab
             * @param position the index of the tab selected
             */
            @Override
            public void onPageSelected(int position) {
                MenuItem item = navigationMenu.getItem(position);
                for(int i=0; i<3;i++){
                    navigationMenu.getItem(i).setChecked(false);
                }

                item.setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });

        //Create bottom tabs and set a listener for when a tab is moved to
        BottomNavigationView tabs = (BottomNavigationView) findViewById(R.id.navigation);
        tabs.setOnNavigationItemSelectedListener(tabsListener);
        navigationMenu = tabs.getMenu();

        dataPrefs = getSharedPreferences("dataPrefs",Context.MODE_PRIVATE);
    }

    /**
     * Called when activity is in foreground. Here the background service is stopped and its data
     * can be extracted and processed by the app
     */
    @Override
    protected void onResume() {
        super.onResume();
        if(isServiceBound){
            stopService(new Intent(BackGroundService.BG_SERVICE));
            isServiceBound=false;
        }

        mSensorManager.registerListener(this, stepDetector,SensorManager.SENSOR_DELAY_FASTEST);
    }

    /**
     * Called when this activity is no longer in the foreground. In our case, this will only be called
     * when the app is put in the background or the device becomes locked (since we only have one activity)
     */
    @Override
    protected void onPause() {
        super.onPause();
        Calendar c = Calendar.getInstance();

        /*
        Getting various health data and saving it
         */

        YearHealthData data = tabFragment.getCurrentYearData();
        data.getMonthHealthData(c.get(Calendar.MONTH)).daysInMonthData.get(c.get(Calendar.DAY_OF_MONTH )-1).steps+=currentNumOfSteps;
        data.getMonthHealthData(c.get(Calendar.MONTH)).totalSteps+=currentNumOfSteps;
        data.totalSteps+=currentNumOfSteps;

        data.getMonthHealthData(c.get(Calendar.MONTH)).daysInMonthData.get(c.get(Calendar.DAY_OF_MONTH )-1).distanceTravelled+=myMapFrag.distTravelled;
        data.getMonthHealthData(c.get(Calendar.MONTH)).totalDistTravelled+=myMapFrag.distTravelled;
        data.totalDist+=myMapFrag.distTravelled;

        data.getMonthHealthData(c.get(Calendar.MONTH)).daysInMonthData.get(c.get(Calendar.DAY_OF_MONTH )-1).routesCompleted+=myMapFrag.routesCompleted;
        data.getMonthHealthData(c.get(Calendar.MONTH)).totalRoutesCompleted+=myMapFrag.routesCompleted;
        data.totalRoutes+=myMapFrag.routesCompleted;

        ArrayList<YearHealthData> tempAllData = tabFragment.getAllData();
        tempAllData.set(tempAllData.size()-1,data);
        dataPrefs.edit().putString("totalData",gson.toJson(tempAllData)).apply();

        mSensorManager.unregisterListener(this, stepDetector);
    }

    /**
     * Scrolls to the fragment view that corresponds to a clicked tab item.
     */
    private BottomNavigationView.OnNavigationItemSelectedListener tabsListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mViewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_map:
                    mViewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_settings:
                    mViewPager.setCurrentItem(2);
                    return true;
            }
            return false;
        }
    };

    /**
     * An implementation of the onSensorChanged interface
     * This is called when one of our sensors (magnetometer or accelerometer) gets a new reading
     * (every several thousand microseconds).
     * @param event An object containing information about the sensor that changed, and its reading
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor == stepDetector){
            currentNumOfSteps++;
            Log.i("numSteps",Integer.toString(currentNumOfSteps));
        }
    }

    //Here because it needs to be implemented
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    /**
     * Creates a list of place types to use for the expandableListViewAdapter
     * @return An adapter-compatible list configuration of placeType data
     */
    public List<List<PlaceTypesWrapper>>getUserPlaceTypesFromFragmentInstance(){
        return settingsFragment.getUserPlaceTypes();
    }

    /**
     * Called when user clicks on generate new route button
     * @param distance radius to search within (in meters)
     * @param numOfLocations number of waypoints to stop at before end location can be reached
     */
    @Override
    public void onButtonClick(int distance, int numOfLocations) {
        myMapFrag.generateRoute(distance,numOfLocations);
    }
}
