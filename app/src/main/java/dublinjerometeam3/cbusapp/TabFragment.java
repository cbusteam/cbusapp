package dublinjerometeam3.cbusapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;

/**
 * Created by Michael on 4/14/2017.
 */

/**
 * Fragment containing graphs for health data over time
 */
public class TabFragment extends Fragment {

    /**
     * An ArrayList containing data about each Graph's state, such as what items have been selected,
     * and the type of data each graph contains
     */
    ArrayList<GraphStateData> stateData = new ArrayList<>();

    /**
     * An ArrayList containing all health data from the time the app was installed on the user's
     * device
     */
    ArrayList<YearHealthData> allData = new ArrayList<>();

    /**
     * The LinearLayout that houses the FrameLayouts of the graphs
     */
    LinearLayout graphParent;

    /**
     * The FrameLayouts that house each graph. These are used to make the graphs fill the screen.
     */
    FrameLayout stepsFrame;
    FrameLayout distanceFrame;
    FrameLayout routesFrame;

    /**
     * Used for serialization
     */
    Gson decoder = new Gson();

    /**
     * The SharedPreferences file where health data is stored
     */
    SharedPreferences dataStoragePrefs;

    /**
     * The current month index (used by some methods when iterating)
     */
    int currentMonth=-1;

    public TabFragment(){

    }

    /**
     * Called when the Fragment's view has been inflated and is ready to use
     * @param view The parent view where all the Fragment's elements are contained
     * @param savedInstanceState Information about the previous state of the layout
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.welcomepanel, container, false);
    }

    /**
     * Verifies if the actual real-life year is the same as the year in the latest YearHealthData
     * Object. If not, it creates a new YearHealthData Object and appends it to the list of YearHealthData
     * Objects in the sharedPrefs file
     */
    public void checkYear(){
        Calendar calendar = Calendar.getInstance();

        String yearDataString=dataStoragePrefs.getString("totalData","");

        if(!yearDataString.equals("")){
            allData = new ArrayList<>(Arrays.asList(decoder.fromJson(yearDataString,YearHealthData[].class)));

            if((calendar.get(Calendar.YEAR))==(allData.get(allData.size()-1).getYear())) {
                Log.i("df","af");
            }

            else{
                allData.add(createNewYear());
                dataStoragePrefs.edit().putString("totalData",decoder.toJson(allData)).apply();
            }
        }

        else{
            allData.add(createNewYear());
            dataStoragePrefs.edit().putString("totalData",decoder.toJson(allData)).apply();
        }
    }

    /**
     * Creates a new YearHealthDataObject and sets up the year's structure
     * @return A formatted YearHealthData Object
     */
    public YearHealthData createNewYear(){
        Calendar calendar = Calendar.getInstance();
        YearHealthData initYearData = new YearHealthData();

        for (int i = 0; i < 12; i++){
            initYearData.monthsHealthData.add(new MonthHealthData());
            calendar.set(Calendar.MONTH,i);

            for (int j = 0; j < calendar.getActualMaximum(Calendar.DAY_OF_MONTH); j++){
                initYearData.monthsHealthData.get(i).daysInMonthData.add(new DayHealthData());
            }
        }

        initYearData.setYear(calendar.get(Calendar.YEAR));
        return initYearData;
    }

    /**
     * Gets the current YearHealthData Object
     * @return The current YearHealthData Object
     */
    public YearHealthData getCurrentYearData(){
        return allData.get(allData.size()-1);
    }

    /**
     * Gets all the app's health data
     * @return An ArrayList of YearHealthData Objects
     */
    public ArrayList<YearHealthData> getAllData(){
        return allData;
    }

    /**
     * Displays each year's total data
     * @param stateData The data for the Graph
     * @param typeOfData The type of data this graph will display
     */
    private void drawAllData(GraphStateData stateData, final int typeOfData){
        final GraphStateData data = stateData;
        String[] labels;
        StaticLabelsFormatter formatter = new StaticLabelsFormatter(data.getGraph());
        DataPoint[] points;

        final int minYear = allData.get(0).getYear();
        int maxYear = allData.get(allData.size()-1).getYear();

        if((maxYear-minYear)+1 > 1){
            points = new DataPoint[(maxYear-minYear)+1];
            labels=new String[(maxYear-minYear)+1];
        }

        else{
            points = new DataPoint[2];
            points[1] = new DataPoint(1,0);

            labels = new String[2];
            labels[0] = Integer.toString(allData.get(0).getYear());
            labels[1] = Integer.toString(allData.get(0).getYear()+1);
        }

        for(int i = minYear; i<=maxYear;i++){
            if(typeOfData==0) {
                points[i - minYear] = new DataPoint(i - minYear,
                        allData.get(i - minYear).totalSteps
                );
            }

            else if(typeOfData==1){
                points[i - minYear] = new DataPoint(i - minYear,
                        allData.get(i - minYear).totalDist
                );
            }

            else{
                points[i - minYear] = new DataPoint(
                        i - minYear,
                        allData.get(i - minYear).totalRoutes
                );
            }

            labels[i-minYear] = Integer.toString(allData.get(i-minYear).getYear());
        }

        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(points);
        series.setDrawValuesOnTop(true);
        series.setValuesOnTopColor(Color.rgb(127,255,212));
        series.setAnimated(true);
        series.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                data.getGraph().removeAllSeries();
                data.setSelState(data.getSelState()+1);

                data.setSelYear(
                        allData.get(
                                (int)dataPoint.getX())
                                .getYear()
                );
                getMonthsData(
                        (int)(dataPoint.getX()+minYear),
                        true,
                        data,
                        typeOfData
                );

                fadeIn(data.getBackButton());
            }
        });

        stateData.getGraph().getGridLabelRenderer().setHorizontalAxisTitle("Year");
        formatter.setHorizontalLabels(labels);
        stateData.getGraph().getGridLabelRenderer().setLabelFormatter(formatter);
        stateData.getGraph().addSeries(series);
    }

    /**
     * Displays each day's data for the given week
     * @param currentDayOfMonth The day of the month to use. The method will automatically calculate
     *                          what week to display given the day
     * @param currentMonth The month selected
     * @param year The year selected
     * @param typeOfData The type of data to display
     *                   0 = steps
     *                   1 = distance
     *                   2 = routes
     * @param draw True if data should be drawn to the given graph
     * @param graph The graph to draw data to
     * @return The sum of the type of data for the week
     */
    public double getWeekData(int currentDayOfMonth,int currentMonth, int year, int typeOfData, boolean draw, @Nullable GraphStateData graph) {
        int yearIndex=-1;
        for(int i=0; i < allData.size();i++){
            if(allData.get(i).getYear()==year){
                yearIndex=i;
                i=allData.size();
            }
        }
        double weekTotalData=0;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,currentMonth);
        calendar.set(Calendar.DAY_OF_MONTH,currentDayOfMonth);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        DataPoint[] points = new DataPoint[7];
        int tempIterator = 0;
        for (int i = currentDayOfMonth - (dayOfWeek - 1)-1; i <= (currentDayOfMonth - (dayOfWeek - 1)) + 5; i++) {
            int index = i;
            int tempCurrentMonth = currentMonth;
            if (i < 0) {
                tempCurrentMonth--;
                index = allData.get(yearIndex).getMonthHealthData(currentMonth - 1).daysInMonthData.size() - Math.abs(i);
            } else if (i >= calendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
                tempCurrentMonth++;
                index = i - calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            }
            if(typeOfData==0){points[tempIterator] = new DataPoint(tempIterator, allData.get(yearIndex).getMonthHealthData(tempCurrentMonth).daysInMonthData.get(index).steps);}
            else if(typeOfData==1){points[tempIterator] = new DataPoint(tempIterator, allData.get(yearIndex).getMonthHealthData(tempCurrentMonth).daysInMonthData.get(index).distanceTravelled);}
            else{points[tempIterator] = new DataPoint(tempIterator, allData.get(yearIndex).getMonthHealthData(tempCurrentMonth).daysInMonthData.get(index).routesCompleted);}
            weekTotalData+=points[tempIterator].getY();
            tempIterator++;
        }
        if(draw) {
            StaticLabelsFormatter formatter = new StaticLabelsFormatter(graph.getGraph());
            BarGraphSeries<DataPoint> series = new BarGraphSeries<>(points);
            graph.getGraph().removeAllSeries();
            graph.getGraph().getGridLabelRenderer().setHorizontalAxisTitle("Day of week");
            series.setAnimated(true);
            series.setDrawValuesOnTop(true);
            series.setValuesOnTopColor(Color.rgb(127,255,212));
            series.setSpacing(10);
            graph.getGraph().addSeries(series);
            formatter.setHorizontalLabels(new String[]{"Su", "M", "Tu", "W", "Th", "F", "Sa"});
            graph.getGraph().getGridLabelRenderer().setLabelFormatter(formatter);
        }
        return weekTotalData;
    }

    /**
     * Displays the data for each week in the selected month
     * @param month The selected month
     * @param year The selected year
     * @param typeOfData The type of data to display
     *                   0 = steps
     *                   1 = distance
     *                   2 = routes
     * @param draw True if the specified graph should display this data
     * @param graph The graph to draw to
     * @return The sum of all data for the month
     */
    public double getWeeksDataForMonth(int month, final int year, final int typeOfData, boolean draw, @Nullable final GraphStateData graph){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MONTH,month);

        int dayOfMonth = 1;
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        c.set(Calendar.YEAR, year);

        DataPoint[] points = new DataPoint[c.getActualMaximum(Calendar.WEEK_OF_MONTH)];
        String[] dates = new String[c.getActualMaximum(Calendar.WEEK_OF_MONTH)];

        double totalMonthData =0;

        for(int i = 0; i < c.getActualMaximum(Calendar.WEEK_OF_MONTH);i++){
            double tempData=getWeekData(dayOfMonth,month,year,typeOfData,false,null);
            dayOfMonth+=c.getActualMaximum(Calendar.DAY_OF_WEEK_IN_MONTH)+1;

            c.set(Calendar.DAY_OF_MONTH,dayOfMonth);
            if(draw){
                dates[i] = ""+(i+1);
                points[i] = new DataPoint(i,tempData);
            }

            totalMonthData+=tempData;
        }

        if(draw){
            StaticLabelsFormatter formatter = new StaticLabelsFormatter(graph.getGraph());
            formatter.setHorizontalLabels(dates);
            BarGraphSeries<DataPoint> series = new BarGraphSeries<>(points);

            series.setAnimated(true);
            series.setDrawValuesOnTop(true);
            series.setSpacing(10);
            series.setValuesOnTopColor(Color.rgb(127,255,212));
            series.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    graph.getGraph().removeAllSeries();
                    graph.setSelState(graph.getSelState()+1);

                    getWeekData(
                            ((int)dataPoint.getX()*7)-7,
                            currentMonth,
                            year,
                            typeOfData,
                            true,
                            graph
                    );
                }
            });

            graph.getGraph().getGridLabelRenderer().setHorizontalAxisTitle("Week number");
            graph.getGraph().addSeries(series);
            graph.getGraph().getGridLabelRenderer().setLabelFormatter(formatter);
        }
        return totalMonthData;
    }

    /**
     * Displays and sums data for each month
     * @param year The selected year
     * @param draw True if specified graph should draw this data
     * @param graph The data for the graph to draw to
     * @param typeOfData The type of data to use
     *                   0 = steps
     *                   1 = distance
     *                   2 = routes
     * @return The sum of all typeofdata for the year
     */
    public double getMonthsData(final int year, boolean draw, @Nullable final GraphStateData graph, final int typeOfData){
        double totalData = 0;
        DataPoint[] points = new DataPoint[12];

        for(int i=0; i<=11;i++){
            double tempData=getWeeksDataForMonth(i,year,typeOfData,false,null);

            if(draw){
                points[i] = new DataPoint(i, tempData);
            }

            totalData+=tempData;
        }
        if(draw){
            StaticLabelsFormatter formatter = new StaticLabelsFormatter(graph.getGraph());
            BarGraphSeries<DataPoint> series = new BarGraphSeries<>(points);
            series.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    currentMonth = (int) dataPoint.getX();
                    graph.getGraph().removeAllSeries();
                    graph.setSelMonth(currentMonth);
                    getWeeksDataForMonth(currentMonth,year,typeOfData,true,graph);
                }
            });

            series.setAnimated(true);
            series.setDrawValuesOnTop(true);
            series.setSpacing(10);
            series.setValuesOnTopColor(Color.rgb(127,255,212));

            graph.getGraph().getGridLabelRenderer().setHorizontalAxisTitle("Month");
            graph.getGraph().addSeries(series);
            formatter.setHorizontalLabels(new String[]{"J", "F", "M", "A", "M", "J", "J","A","S","O","N","D"});
            graph.getGraph().getGridLabelRenderer().setLabelFormatter(formatter);
        }

        return totalData;
    }

    /**
     * Initializes all the views and GraphStateData Objects
     * @param view The inflated parent view
     */
    private void init(View view){

        dataStoragePrefs = getActivity().getSharedPreferences("dataPrefs",Context.MODE_PRIVATE);

        checkYear();

        graphParent = (LinearLayout) view.findViewById(R.id.graphparentlayout);

        stepsFrame = (FrameLayout) view.findViewById(R.id.stepsframe);
        distanceFrame = (FrameLayout) view.findViewById(R.id.distanceframe);
        routesFrame = (FrameLayout) view.findViewById(R.id.routesframe);

        ViewTreeObserver observer = graphParent.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                graphParent.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                int wid = graphParent.getMeasuredWidth();
                int height = graphParent.getMeasuredHeight();

                stepsFrame.setLayoutParams(new LinearLayout.LayoutParams(wid,height));
                distanceFrame.setLayoutParams(new LinearLayout.LayoutParams(wid,height));
                routesFrame.setLayoutParams(new LinearLayout.LayoutParams(wid,height));
            }
        });

        GraphView stepsGraph = (GraphView) view.findViewById(R.id.stepsgraph);
        GraphView distGraph = (GraphView) view.findViewById(R.id.distgraph);
        GraphView routesGraph = (GraphView) view.findViewById(R.id.routesgraph);
        Button stepsBackButton = (Button) view.findViewById(R.id.stepsbackbutton);
        Button distBackButton = (Button) view.findViewById(R.id.distancebackbutton);
        Button routesBackButton = (Button) view.findViewById(R.id.routesbackbutton);

        Collections.addAll(stateData,
                new GraphStateData(stepsGraph,stepsBackButton),
                new GraphStateData(distGraph,distBackButton),
                new GraphStateData(routesGraph,routesBackButton)
                );

        initGraph(stateData.get(0).getGraph(),"Steps taken");
        initGraph(stateData.get(1).getGraph(),"Distance travelled (m)");
        initGraph(stateData.get(2).getGraph(),"Routes completed");

        for(int i=0; i<3;i++){
            final int ind =i;
            stateData.get(i).getBackButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    stateData.get(ind).getGraph().removeAllSeries();
                    switch (stateData.get(ind).getSelState()){
                        case 0:
                            stateData.get(ind).setSelYear(-1);
                            fadeOut(stateData.get(ind).getBackButton());
                            drawAllData(stateData.get(ind),ind);
                            break;
                        case 1:
                            getMonthsData(stateData.get(ind).getSelYear(),true,stateData.get(ind),ind);
                            stateData.get(ind).setSelMonth(-1);
                            break;
                        case 2:
                            getWeeksDataForMonth(
                                    stateData.get(ind).getSelMonth(),
                                    stateData.get(ind).getSelYear(),
                                    ind,
                                    true,
                                    stateData.get(ind)
                            );
                            break;
                    }
                    stateData.get(ind).setSelState(stateData.get(ind).getSelState()-1);
                }
            });
            drawAllData(stateData.get(i),i);
        }
    }

    /**
     * Initializes the graph data for the given GraphView
     * @param graph The GraphView to initialize
     * @param title The title of the graph
     */
    private void initGraph(GraphView graph, String title){
        graph.setTitle(title);
        graph.setBackgroundColor(Color.LTGRAY);
        graph.getViewport().setScrollable(true);
        graph.getGridLabelRenderer().setVerticalLabelsVisible(false);
        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.HORIZONTAL);
        graph.getGridLabelRenderer().setTextSize(50);
        graph.setTitleTextSize(65);
    }

    /**
     * Animated fade button out
     * @param button The button to apply this animation to
     */
    private void fadeOut(final Button button){
        button.setEnabled(false);
        Animation anim = AnimationUtils.loadAnimation(getContext(),R.anim.fadeout);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                button.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });

        button.startAnimation(anim);
    }

    /**
     * Animates fade in for the given button
     * @param button The button to fade in
     */
    private void fadeIn(final Button button){
        button.setVisibility(View.VISIBLE);
        button.setEnabled(false);

        Animation anim = AnimationUtils.loadAnimation(getContext(),R.anim.fadein);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                button.setEnabled(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        button.startAnimation(anim);
    }
}
