package dublinjerometeam3.cbusapp;

import android.widget.Button;

import com.jjoe64.graphview.GraphView;

/**
 * Created by Michael on 4/23/2017.
 */

public class GraphStateData {

    /**
     * The month selected (-1 if none)
     */
    private int selMonth = -1;

    /**
     * The current mode of this graph
     * 0 = displaying years
     * 1 = displaying months
     * 2 = displaying weeks
     * 3 = displaying days
     */
    private int selState= 0;

    /**
     * The year selected (-1 if none)
     */
    private int selYear = -1;

    /**
     * The graph this instance holds
     */
    private GraphView graph;

    /**
     * The back button linked to this specific instance
     */
    private Button backButton;

    GraphStateData(GraphView graph, Button backButton){
        this.graph = graph;
        this.backButton = backButton;
    }

    int getSelMonth() {
        return selMonth;
    }

    void setSelMonth(int selMonth) {
        this.selMonth = selMonth;
    }

    int getSelState() {
        return selState;
    }

    void setSelState(int selState) {
        this.selState = selState;
    }

    int getSelYear() {
        return selYear;
    }

    void setSelYear(int selYear) {
        this.selYear = selYear;
    }

    GraphView getGraph() {
        return graph;
    }

    Button getBackButton() {
        return backButton;
    }
}
