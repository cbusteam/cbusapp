package dublinjerometeam3.cbusapp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Michael on 4/17/2017.
 */

/**
 * Contains data for the entire year
 */
class YearHealthData {
    /**
     * The list of MonthHealthData Objects, indexed from 0 to 11
     */
    List<MonthHealthData> monthsHealthData = new ArrayList<>();

    /**
     * This instance's year
     */
    private int year;

    /**
     * This instance's current data
     */
    int totalSteps=0;
    double totalDist=0;
    int totalRoutes=0;

    void setYear(int year){
        this.year = year;
    }

    int getYear(){
        return year;
    }

    MonthHealthData getMonthHealthData(int month){
        return monthsHealthData.get(month);
    }
}
